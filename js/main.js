const inputs = document.querySelectorAll(".input");


function newclass() {
    let parent = this.parentNode.parentNode;
    parent.classList.add("focus");
}

function oldclass() {
    let parent = this.parentNode.parentNode;
    if (this.value == "") {
        parent.classList.remove("focus");
    }
}


inputs.forEach(input => {
    input.addEventListener("focus", newclass);
    input.addEventListener("blur", oldclass);
});